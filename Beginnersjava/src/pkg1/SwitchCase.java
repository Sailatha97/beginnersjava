package pkg1;
import java.util.Scanner;

public class SwitchCase {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("enter two integers");
		int a = sc.nextInt();
		int b = sc.nextInt();
		System.out.println("enter an option from 1 to 4");
		int ch = sc.nextInt();
		int x = operations(a, b, ch);
		System.out.println(x);
		sc.close();
	}

	public static int operations(int a, int b, int ch) {
		int x = 0;
		switch (ch) {
		case 1:
			x = a + b;
			break;
		case 2:
			x = a - b;
			break;
		case 3:

			x = a * b;
			break;
		case 4:
			x = a / b;
			break;
		}
		return x;

	}

}

