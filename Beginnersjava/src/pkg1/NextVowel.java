package pkg1;

public class NextVowel {

	public static void main(String[] args) {
		String str = "kcbsdhvgyeJYFDTYEFG";
		str = str.toLowerCase();
		String str2 = " ";
		String vowels = "aeioua";
		System.out.println(generateNextVowel(str, vowels, str2));

	}

	public static String generateNextVowel(String str, String vowels, String str2) {
		for (int i = 0; i <= str.length()-1; i++) {
			char c = str.charAt(i);
			if (vowels.indexOf(c) != -1)
				c = vowels.charAt(vowels.indexOf(c + 1));
			str2 = str2 + c;

		}

		return str2;
	}

}
