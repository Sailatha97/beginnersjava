package pkg1;

import java.util.Scanner;

public class NumberLength {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number");
		int num = sc.nextInt();

		int length = calcOfDigits(num);
		if (length == 5)
			System.out.println(num + " is a five digit number");
		else
			System.out.println(num + "is not a five digit number");
		sc.close();

	}

	public static int calcOfDigits(int num) {
		String number = num + "";
		return number.length();

	}

}
