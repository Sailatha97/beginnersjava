package pkg1;
import java.util.Scanner;

public class SumOfAllDigits {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("enter a number");
		int num = sc.nextInt();
		System.out.println("The number is" + num);
		int sum = calcSumOfDigits(num);
		System.out.println("The sum of digits is" + sum);
		sc.close();
	}

	public static int calcSumOfDigits(int num) {
		int sum = 0;
		while (num > 0) {
			int d1 = num % 10;
			System.out.println(d1);
			sum = sum + d1;
			num = num / 10;
		}
		return sum;

	}

}
