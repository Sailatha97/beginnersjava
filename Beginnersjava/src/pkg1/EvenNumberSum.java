package pkg1;

public class EvenNumberSum {
	public static void main(String[] args) {
		int sum = sumOfNumbers();
		System.out.println(sum);

	}

	public static int sumOfNumbers() {
		int num = 1, sum = 0;
		while (num <= 1000) {
			if (num % 2 == 0) {
				sum = sum + num;
			}
			num++;
		}
		return sum;
	}
}
