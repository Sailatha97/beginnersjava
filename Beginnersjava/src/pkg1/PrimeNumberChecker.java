package pkg1;

import java.util.Scanner;

public class PrimeNumberChecker {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		int num = sc.nextInt();

		int count = factors(num);
		boolean result = isPrime(count);
		System.out.println(result);
		sc.close();
	}

	public static int factors(int num) {

		int count = 0;
		for (int i = 1; i <= num; i++) {
			if (num % i == 0)
				count++;
		}
		return count;

	}

	public static boolean isPrime(int count) {
		if (count == 2)
			return true;
		else
			return false;
	}
}
