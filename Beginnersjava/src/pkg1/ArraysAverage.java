package pkg1;

import java.util.Scanner;

public class ArraysAverage {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the marks");
		int marks[] = new int[5];
		for (int i = 0; i < 5; i++)
			marks[i] = sc.nextInt();

		System.out.println("Average obtained  is " + result(marks));
		sc.close();
	}

	public static float result(int marks[]) {
		int sum = 0;

		for (int i = 0; i < 5; i++)
			sum = sum + marks[i];
		return sum / 5.0f;
	}

}
