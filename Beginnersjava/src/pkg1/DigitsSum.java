package pkg1;

import java.util.Scanner;

public class DigitsSum {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a number");
		int num = sc.nextInt();
		int sum = sumOfDigits(num);
		System.out.println(sum);
		int dif = diffOfDigits(num);
		System.out.println(dif);
		sc.close();

	}

	public static int sumOfDigits(int num) {
		int d1 = num % 10;
		int d2 = num / 10;
		int sum = d1 + d2;
		return sum;

	}

	public static int diffOfDigits(int num) {
		int d1 = num % 10;
		int d2 = num / 10;
		int dif = d1 - d2;
		return dif;
	}

}
