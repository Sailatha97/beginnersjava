package pkg1;

import java.util.Scanner;

public class ChangeCaseOfString {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a string ");
		String str = sc.nextLine();
		String str1 = changeLowerToUpper(str);
		System.out.println(str1);
		sc.close();
	}

	public static String changeLowerToUpper(String str) {
		String str1=" ";
		
		for (int i = 0; i <= str.length()-1; i++) {
			char ch = str.charAt(i);
			if (Character.isUpperCase(ch))
				System.out.println(Character.toLowerCase(ch));
			else
				System.out.println(Character.toUpperCase(ch));
			str1=str+ch;
		}
		
		
		return str1;

	}
}
