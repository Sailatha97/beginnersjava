package pkg1;

import java.util.Scanner;

public class ArrayOrder {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the values");
		int arr[] = new int[7];
		for (int i = 0; i < 7; i++)
			arr[i] = sc.nextInt();

		arr = ascendingOrder(arr);
		System.out.println("the Array after sorting is:");
		for (int i = 0; i < 7; i++) {
			System.out.println(arr[i]);
			sc.close();

		}

	}

	public static int[] ascendingOrder(int arr[]) {
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 6; j++) {
				if (arr[j] > arr[j + 1]) {
					int temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
			}
		}
		return arr;

	}
}
