package pkg1;
import java.util.Scanner;

public class CountOfOddEven {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("enter a number");
		int num = sc.nextInt();
		calcCountOfEvenAndOdd(num);
		sc.close();
	}

	public static void calcCountOfEvenAndOdd(int num) {
		int count = 0, count1 = 0;
		while (num > 0) {
			int d1 = num % 10;
			if (d1 % 2 == 0) {

				count++;
			} else {

				count1++;
			}
			num = num / 10;
		}

		System.out.println("count of even digits" + count);
		System.out.println("count of odd digits" + count1);

	}

}
