package pkg1;

public class ContinueBreak {

	public static void main(String[] args) {
		int i = 0;
		while (i < 10) {
			if (i == 5) {
				i++;
				break;
				// continue;
			}
			System.out.println(i);
			i++;

		}

	}

}
