package pkg1;

import java.util.Scanner;

public class FactorialGenerator {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		int num = sc.nextInt();

		int value = factorial(num);
		System.out.println("The factorial is" + value);
		sc.close();
	}

	public static int factorial(int num) {
		int value = 1;
		for (int i = 1; i <= num; i++) {
			value = value * i;

		}
		return value;
	}
}
