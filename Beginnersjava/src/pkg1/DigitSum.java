package pkg1;

public class DigitSum {
	public static void main(String[] args) {
		int num = 799;
		System.out.println(getSumOfDigits(num));
	}

	public static int getSumOfDigits(int num) {
		if (num < 10 || num > 99)
			return 0;

		return num / 10 + num % 10;

	}

}
