package pkg1;
import java.util.Scanner;

public class Circle {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the value of r");
		float r = sc.nextFloat();
		float area = areaOfCircle(r);
		float perimeter = perimeterOfCircle(r);
		System.out.println(area);
		System.out.println(perimeter);
		sc.close();
	}

	public static float areaOfCircle(float r) {
		float area = (3.14f * r * r);
		return area;

	}

	public static float perimeterOfCircle(float r) {
		float perimeter = (2 * 3.14f * r);
		return perimeter;

	}
}
