package pkg1;

import java.util.Scanner;

public class VowelsInString {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a string to count vowels");
		String str = sc.nextLine();
		int count = CountOfVowels(str);
		System.out.println(count);
		sc.close();

	}

	public static int CountOfVowels(String str) {
		int count = 0;
		for (int i = 0; i <= str.length() - 1; i++) {
			if (str.charAt(i) == 'a')
				count++;
			if (str.charAt(i) == 'e')
				count++;
			if (str.charAt(i) == 'i')
				count++;
			if (str.charAt(i) == 'o')
				count++;
			if (str.charAt(i) == 'u')
				count++;

		}
		return count;
	}
}