package pkg1;

import java.util.Scanner;

public class CountOfDigits {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("enter a number");
		int num = sc.nextInt();
		System.out.println("The number is" + num);
		int count = calcDigitCount(num);
		System.out.println("The number of digits is" + count);
		sc.close();
	}

	public static int calcDigitCount(int num) {
		int count = 0;
		while (num > 0) {
			int d1 = num % 10;
			System.out.println(d1);
			count++;
			num = num / 10;

		}
		return count;

	}

}
