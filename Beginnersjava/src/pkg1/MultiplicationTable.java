package pkg1;

public class MultiplicationTable {

	public static void main(String[] args) {
		int i = 1;
		for (i = 1; i <= 10; i++) {
			int d = 10 * i;
			System.out.printf("10 *%d = %d\n", i, d);
		}

	}

}
