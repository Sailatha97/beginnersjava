package pkg1;

import java.util.Scanner;

public class DaysOfWeek {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a  number");
		int num = sc.nextInt();
		String day = nameOfDay(num);
		System.out.println(day);
		sc.close();

	}

	public static String nameOfDay(int num) {
		String day = "";
		switch (num) {
		case 1:
			day = "sunday";
			break;
		case 2:
			day = "monday";
			break;
		case 3:
			day = "tuesday";
			break;
		case 4:
			day = "wednesday";
			break;
		case 5:
			day = "thursday";
			break;
		case 6:
			day = "friday";
			break;
		case 7:
			day = "saturday";
			break;
		}
		return day;
	}

}
