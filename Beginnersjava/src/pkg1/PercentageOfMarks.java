package pkg1;

import java.util.Scanner;

public class PercentageOfMarks {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter three subject marks");
		float num1 = sc.nextFloat();
		float num2 = sc.nextFloat();
		float num3 = sc.nextFloat();
		float percentage = calcPercent(num1, num2, num3);
		System.out.println("the percentage is" + percentage);
		char grade = calcGrade(percentage);
		System.out.println("the grade is" + grade);
		sc.close();

	}

	public static float calcPercent(float num1, float num2, float num3) {
		float percentage = (num1 + num2 + num3) / 300 * 100;
		return percentage;

	}

	public static char calcGrade(float percentage) {
		if (percentage >= 90) {
			char grade = 'A';
			return grade;
		} else if (percentage >= 70 && percentage <= 90) {
			char grade = 'B';
			return grade;
		} else if (percentage >= 50 && percentage <= 70) {
			char grade = 'C';
			return grade;
		}
		if (percentage >= 50) {
			char grade = 'D';
			return grade;
		}
		return 0;

	}
}
