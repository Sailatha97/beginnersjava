package pkg1;

import java.util.Scanner;

public class SpecialCharacterSeperator {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a string ");
		String str = sc.nextLine();
		String str3 = separateSpecialCharacters(str);
		System.out.println(str3);
		sc.close();

	}

	public static String separateSpecialCharacters(String str) {
		String str1 = " ";
		String str2 = " ";
		String str3 = " ";
		for (int i = 0; i <= str.length() - 1; i++) {
			if (Character.isAlphabetic(str.charAt(i)))
				str1 = str1 + str.charAt(i);
			else if(Character.isDigit(str.charAt(i)))
				str2 = str2 + str.charAt(i);
			else
				str3=str3+str.charAt(i);

		}
		str3 = str3+str2+str1;
		return str3;
	}

}