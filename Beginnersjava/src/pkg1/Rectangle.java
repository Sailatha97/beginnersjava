package pkg1;

import java.util.Scanner;

public class Rectangle {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter values of l and b");
		int l = sc.nextInt();
		int b = sc.nextInt();
		int area = areaOfRect(l, b);
		int perimeter = perimeterOfRectangle(l, b);
		System.out.println(area);
		System.out.println(perimeter);
		sc.close();

	}

	public static int areaOfRect(int l, int b) {
		int area = l * b;

		return area;

	}

	public static int perimeterOfRectangle(int l, int b) {
		int perimeter = 2 * (l + b);
		return perimeter;
	}

}
