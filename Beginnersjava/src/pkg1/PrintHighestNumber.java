package pkg1;

import java.util.Scanner;

public class PrintHighestNumber {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("enter a number");
		int num = sc.nextInt();
		System.out.println("The number is" + num);
		int highestnumber = calcHighestNumber(num);
		System.out.println("The highest digit is" + highestnumber);
		sc.close();
	}

	public static int calcHighestNumber(int num) {
		int max = 0;
		while (num > 0) {
			int d1 = num % 10;
			System.out.println(d1);
			if (max < d1)
				max = d1;
			num = num / 10;

		}
		return max;
	}
}
