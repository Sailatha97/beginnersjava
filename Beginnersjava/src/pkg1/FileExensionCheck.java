package pkg1;

import java.util.Scanner;

public class FileExensionCheck {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter two string");
		String str1 = sc.nextLine();
		String str2 = sc.nextLine();
		String a = "java";
		
		System.out.println(checkExtension(str1, str2, a));
		sc.close();

	}

	public static int checkExtension(String str1, String str2, String a) {
		String[] str3 = str1.split("\\.");
		String[] str4 = str2.split("\\.");

		if (str3[1].equalsIgnoreCase(a) && str4[1].equalsIgnoreCase(a))
			return 2;
		else if (str3[1].equalsIgnoreCase(a) || str4[1].equalsIgnoreCase(a))
			return 1;
		else
			return 0;

	}
}
