package pkg1;

import java.util.Scanner;

public class ReplaceCharacters {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a string ");
		String str = sc.nextLine();
		String str1 = replaceCharacter(str);
		System.out.println(str1);
		sc.close();

	}

	public static String replaceCharacter(String str) {
		String str1 = " ";
		for (int i = 0; i <= str.length() - 1; i++) {
			char ch = str.charAt(i);
			if (ch == 'D')
				ch = 'T';
			if (ch == 'd')
				ch = 't';
			str1 = str1 + ch;

		}
		return str1;

	}

}
