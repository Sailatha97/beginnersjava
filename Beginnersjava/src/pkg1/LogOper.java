package pkg1;

public class LogOper {

	public static void main(String[] args) {

		/*
		 * int num=10,num1=20,num2=30;
		 * 
		 * System.out.println(num < num1 && num < num2); System.out.println(num
		 * > num1 && num < num2); System.out.println(num < num1 && num > num2);
		 * System.out.println(num < num1 || num < num2); System.out.println(num
		 * < num1 || num < num2); System.out.println(num < num1 || num > num2);
		 * System.out.println(num > num1 || num > num2);
		 */
		int a = 10, b = 20, c = 30;
		// System.out.println(a>20 && ++b <30 && ++c >30);
		// System.out.println(a>20 & ++b<30 & ++c>30);
		// System.out.println(a>20 || ++b <30 || ++c>30);
		System.out.println(a > 20 | ++b < 30 | ++c > 30);

		System.out.println(b);
		System.out.println(c);

	}

}
